package com.thed.zapi.rest.vo;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

import java.util.List;
import java.util.Map;

/**
 * Created by smangal on 2/1/14.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class ExecutionResponseVO {
    public Map<String, StatusVO> status;
    public List<ExecutionVO> executions;
    public Integer recordsCount;
}
